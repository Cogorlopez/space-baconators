import { Typography } from '@mui/material'
import Grid from '@mui/material/Grid'
import { useShow } from '../../hooks/useShow'
import AttendanceToggle from './AttendanceToggle'
import { useAuth } from 'react-oidc-context'
import { Link } from 'react-router-dom'


const Attendees = ({ showDate }) => {
  const { attendance } = useShow()
  const auth = useAuth()

  const userId = auth.user?.profile['cognito:username'] as string

  return (
    <Grid
      container
      spacing={0}
      direction="column"
      alignItems="center"
      justifyContent="center"
      my={2}
    >
      <Grid item xs={3} justifyContent="center">
        {auth.isAuthenticated && <AttendanceToggle isInAttedanceList={attendance[showDate]?.includes(userId)} showDate={showDate} userId={userId} />}
        <h3>Baconators in the Crowd</h3>
        {showDate && attendance[showDate]?.map(attendee =>
          <Typography align="center" fontSize='22px'>
            <Link
              to={`/profile/${attendee}`}
              style={{
                color: 'rgb(255, 255, 255)',
                textDecoration: 'none'
              }}>
              {attendee}
            </Link>
          </Typography>
        )}
      </Grid>
    </Grid>
  )
}

export default Attendees