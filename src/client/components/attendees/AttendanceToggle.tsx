import Button from '@mui/material/Button/Button'
import ConfirmationNumberIcon from '@mui/icons-material/ConfirmationNumber';
import RemoveCircleOutlineIcon from '@mui/icons-material/RemoveCircleOutline';
import { Box, Stack } from '@mui/material';
import { ENDPOINTS } from '../../../config/default';
import { useDispatch } from 'react-redux';
import { TYPES } from '../../redux/actions/shows';

const AttendanceToggle = ({ isInAttedanceList, showDate, userId }) => {
  const dispatch = useDispatch()
  const handleAdd = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_SERVERLESS_API}${ENDPOINTS.ATTENDEES}`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({showId: showDate, userId})
      })

      if (!response.ok) {
        throw new Error(await response.json())
      }

      const { users } = await response.json()
      console.log(users)
      dispatch({ type: TYPES.SET_ATTENDANCE, attendanceList: users, showDate });
    } catch (error) {
      console.error(error)
    }
  }

  const handleRemove = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_SERVERLESS_API}${ENDPOINTS.ATTENDEES}`, {
        method: 'DELETE',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({showId: showDate, userId})
      })

      if (!response.ok) {
        throw new Error(await response.json())
      }

      const { users } = await response.json()
      console.log(users)
      dispatch({ type: TYPES.SET_ATTENDANCE, attendanceList: users, showDate });
    } catch (error) {
      console.error(error)
    }
  }


  return (
    <Stack direction="row" justifyContent="center">
      {!isInAttedanceList ? <Box my={2}>
        <Button variant="contained" sx={{ backgroundColor: '#006700', color: '#fff' }} onClick={handleAdd}><ConfirmationNumberIcon fontSize="small" sx={{marginRight: '0.25rem'}} />I Went</Button>
      </Box> :
      <Box my={2}>
        <Button variant="contained" sx={{ backgroundColor: '#ba0000', color: '#fff' }} onClick={handleRemove}><RemoveCircleOutlineIcon fontSize="small" sx={{marginRight: '0.25rem'}} />I Didn't Go</Button>
      </Box>}
    </Stack>
  )
}

export default AttendanceToggle