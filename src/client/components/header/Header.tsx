import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import { Link, useHistory } from 'react-router-dom'
import { useAuth } from "react-oidc-context";
import PersonIcon from '@mui/icons-material/Person';

import logo from '../../../baconators-left-cropped.png'


const Logo = () => <a href='/'>
  <img
    src={logo}
    height={50}
    alt="Space Baconators logo"
    />
</a>


const  ResponsiveAppBar = () => {
  const auth = useAuth()
  const history = useHistory()
  const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(null);
  const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(null);

  const handleLogout = () => {
    auth.removeUser()
    history.push('/')
  }

  const username = auth.user?.profile['cognito:username'] as string | undefined
  
  const pages = [{id: 'shows' , link: <Link to={'/shows'} style={{color: 'rgb(255, 255, 255)', textDecoration: 'none', fontSize: '16px'}} >Shows</Link>},
    process.env.REACT_APP_IS_REGISTRATION_ENABLED === 'true' && !auth.isAuthenticated ? {id: 'register' , link: <span onClick={() => auth.signinRedirect()}><Typography style={{color: 'rgb(255, 255, 255)'}} >Login</Typography></span>} : null
  ].filter(page => page !== null);
  const settings = [<Link to={`/profile/${username}`} style={{color: 'rgb(255, 255, 255)', textDecoration: 'none'}} >{username}</Link>,  <span onClick={handleLogout}>Logout</span>].filter(page => page !== null);

  const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <AppBar position="static">
      <Container maxWidth="xl">
        <Toolbar disableGutters>
        <Box sx={{ display: { xs: 'none', md: 'flex' }, mr: 1 }}>
          <Logo />
        </Box>

          <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: 'block', md: 'none' },
              }}
            >
              {pages.map((page) => (
                <MenuItem key={page?.id} onClick={handleCloseNavMenu}>
                  <Typography textAlign="center">{page?.link}</Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>
          <Box flexGrow={1} sx={{ display: { xs: 'flex', md: 'none' }, mr: 1 }}>
            <Logo />
          </Box>
          <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
            {pages.map((page) => (
              <Button
                key={page?.id}
                onClick={handleCloseNavMenu}
                sx={{ my: 2, color: '#fff', display: 'block' }}
              >
                {page?.link}
              </Button>
            ))}
          </Box>
          {auth.isAuthenticated && <Box sx={{ flexGrow: 0 }}>
            <Tooltip title="Open settings">
              <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                <PersonIcon />
              </IconButton>
            </Tooltip>
            <Menu
              sx={{ mt: '45px' }}
              id="menu-appbar"
              anchorEl={anchorElUser}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}
            >
              {settings.map((setting, index) => (
                <MenuItem key={index} onClick={handleCloseUserMenu}>
                  <Typography textAlign="center">{setting}</Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>}
        </Toolbar>
      </Container>
    </AppBar>
  );
}
export default ResponsiveAppBar;