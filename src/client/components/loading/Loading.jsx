import CircularProgress from '@mui/material/CircularProgress'
import { useSelector } from 'react-redux'
import { getIsLoading } from '../../redux/selectors/ui'

const Loading = ({ children }) => {
  const isLoading = useSelector(state => getIsLoading(state))

  if (isLoading) {
    return (
      <div className='d-flex justify-content-center mt-3'>
        <CircularProgress size={100} />
      </div>
    )
  }

  return children
}

export default Loading
