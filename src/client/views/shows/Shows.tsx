import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';

import { Link } from 'react-router-dom'
import { ENDPOINTS } from '../../../config/default'
import { useShow } from '../../hooks/useShow'

const Shows = () => {
  const { shows } = useShow()

  return (
    <div>
      <h1>Shows</h1>
        <Table >
          <TableBody>
            {shows.length > 0 &&
              shows.map(show => (
                <TableRow key={show.date}>
                  <TableCell>
                    <Link
                      to={`${ENDPOINTS.SHOWS}/${show.date}`}
                      key={show.date}
                    >
                      {show.date}
                    </Link>
                  </TableCell>
                  <TableCell>{show.venue.name}</TableCell>
                  <TableCell>{show.venue.city}</TableCell>
                  <TableCell>{show.venue.state}</TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
    </div>
  )
}

export default Shows
