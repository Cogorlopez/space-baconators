import { useAuth } from "react-oidc-context";
import { Redirect } from 'react-router-dom';
const Callback = () => {
const auth = useAuth()
  return (
    auth.isAuthenticated ?
    <Redirect to="/" /> : <div>Redirecting</div>
  )
}

export default Callback