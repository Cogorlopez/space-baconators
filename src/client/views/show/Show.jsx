import { useEffect, useState } from 'react'
import Divider from '@mui/material/Divider';
import Set from './Set'
import ShowHeader from './ShowHeader/ShowHeader'
import { useDispatch } from 'react-redux'
import { useParams } from 'react-router-dom'
import { fetchShowAttendance, setDisplayShowByDate } from '../../redux/actions/shows'
import { useShow } from '../../hooks/useShow'
import  Attendees  from '../../components/attendees/Attendees'
import { Typography } from '@mui/material';

const Show = () => {
  const dispatch = useDispatch()
  const { id: showDate } = useParams()
  const { displayShow, hasShows, attendance } = useShow()
  const [showNotes, setShowNotes] = useState()

  
  const { sets } = displayShow || {}
  
  useEffect(() => {
    if (hasShows) {
      dispatch(setDisplayShowByDate(showDate))
    }
    
    if (displayShow && !Object.keys(attendance).includes(displayShow?.date)) {
      dispatch(fetchShowAttendance(displayShow?.date))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [showDate, hasShows, displayShow, dispatch])

  const getNotes = () => sets?.reduce((result, set) => {
    const songsWithNotes = set?.songs?.filter(song => song.notes)

    songsWithNotes.forEach(song => {
      result[song.name] = song.notes
    });
    
    return result
  }, {})

   useEffect(() => {
    const notes = getNotes()
    if(notes) {
      setShowNotes(notes)
    }
    
   }, [sets])

  return (
    <div className='d-flex flex-column my-4' style={{ width: '100%' }}>
      {sets && (
        <>
          <ShowHeader show={displayShow} />
          <div>
            {sets &&
              sets.map(set => <Set key={set.name} set={set} showNotes={showNotes} />)}
          </div>
          {showNotes && <div>
            <Typography>Notes:</Typography>
            <ol>
              {Object.values(showNotes).map((notes, index) => 
                <li key={index}>{notes.join(", ")}</li>
              )}
            </ol>
          </div>}
          {process.env.REACT_APP_IS_REGISTRATION_ENABLED === 'true' && 
          (<>
            <Divider variant="middle" color='rgb(217,217,217)'/>
            <Attendees showDate={displayShow?.date} />
          </>)
          }
        </>
      )}
    </div>
  )
}

export default Show
