const getSongNameFromId = (songId) => {
  if (typeof songId !== 'string') return ''

  if (!songId.includes('-')) {
    return songId.charAt(0).toUpperCase() + songId.slice(1)
  } else {
    const name = songId.replace('_', ' ')
    return name.replace(/\w\S*/g, (txt) => { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase() })
  }
}

module.exports = {
  getSongNameFromId
}
