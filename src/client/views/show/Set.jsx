import Box from '@mui/material/Box';

const Set = ({ set, showNotes }) => {
  const { name, songs } = set
  const localShowNotes = {
    ...showNotes
  }
  const songNames = songs?.map(song => song.name)
  return (
    <>
    <Box sx={{ display: { xs: 'none', md: 'block' }, mr: 1 }}>
      <h2>{name}</h2>
      <div className='d-flex'>
        <h4 className={'mb-4'}>
          {songs.map(
            (song, index) =>
              `${song.name}${
                song.hasJamOut ? ' > ' : index === songs.length - 1 ? '' : ', '
              }`
          )}
        </h4>
      </div>
    </Box>
    <Box sx={{ display: { xs: 'block', md: 'none' }, mr: 1 }}>
    <h2>{name}</h2>
    <div className='d-flex'>
      <h5 className={'mb-4'}>
        {songs.map(
          // TODO: This whole thing should be its own component
          (song, index) => {
            const { name } = song

            const adjustedNoteIndex = localShowNotes && Object.keys(localShowNotes).includes(name) ?  Object.keys(showNotes).indexOf(name) + 1 : undefined
            
            const displayName = adjustedNoteIndex ? `${name} [${adjustedNoteIndex}]` : name

            adjustedNoteIndex && delete localShowNotes[name]
            console.log(localShowNotes)

            console.log(displayName)
            
            return `${displayName}${
              song.hasJamOut ? ' > ' : index === songs.length - 1 ? '' : ', '
            }`
          }
        )}
      </h5>
    </div>
  </Box>
  </>
  )
}

export default Set
