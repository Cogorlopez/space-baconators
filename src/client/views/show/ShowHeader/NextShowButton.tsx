
import Button from '@mui/material/Button';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import { useHistory } from 'react-router-dom'
import { useShow } from '../../../hooks/useShow'

const NextShowButton = () => {
  const { nextShow, currentShowIndex } = useShow()
  const history = useHistory()
  const handleOnClick = () => {
    history.push(`/shows/${nextShow.date.toString()}`)
  }

  if (currentShowIndex <= 0) {
    return null
  }

  return (
    <>
      <Button variant="contained" onClick={handleOnClick} sx={{backgroundColor: '#6c757d', color: '#fff'}}>
        <ArrowForwardIcon />
      </Button>
    </>
  )
}

export default NextShowButton
