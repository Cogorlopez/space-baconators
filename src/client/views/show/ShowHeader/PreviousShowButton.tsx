import Button from '@mui/material/Button';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { useHistory } from 'react-router-dom'
import { useShow } from '../../../hooks/useShow'

const PreviousShowButton = () => {
  const { previousShow, shows, currentShowIndex } = useShow()
  const history = useHistory()
  const handleOnClick = () => {
    history.push(`/shows/${previousShow.date.toString()}`)
  }

  if (currentShowIndex === shows.length - 1) {
    return null
  }

  return (
    <>
      <Button variant="contained" onClick={handleOnClick} sx={{backgroundColor: '#6c757d', color: '#fff'}}>
        <ArrowBackIcon />
      </Button>
    </>
  )
}

export default PreviousShowButton
