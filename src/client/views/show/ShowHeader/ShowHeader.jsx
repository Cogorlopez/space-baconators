import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import PreviousShowButton from './PreviousShowButton'
import NextShowButton from './NextShowButton'

const ShowHeader = ({ show }) => {
  const { venue, date } = show
  return (
    <>
      <Row className='justify-content-center'>
        <Col className='d-flex justify-content-start'>
          {/* TODO: Wire the buttons */}
          <PreviousShowButton />
        </Col>
        <div className='d-flex justify-content-center'>
          <h2>{date}</h2>
        </div>
        <Col className='d-flex justify-content-end'>
          <NextShowButton />
        </Col>
      </Row>
      <div className='d-flex justify-content-center my-3'>
        <h3>{`${venue.name} | ${venue.city}, ${venue.state}`}</h3>
      </div>
    </>
  )
}

export default ShowHeader
