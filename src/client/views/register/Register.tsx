import { useAuth } from "react-oidc-context";

const Register = () => {
    const auth = useAuth();

    const signOutRedirect = () => {
      const clientId = "86ib79dtrm5ml3u6k887lol7k";
      const logoutUri =( process.env.REACT_APP_BASE_URL ?? 'https://spacebaconators.com') + "/register";
      const cognitoDomain = "https://us-east-1kcnywtk3m.auth.us-east-1.amazoncognito.com";
      window.location.href = `${cognitoDomain}/logout?client_id=${clientId}&logout_uri=${encodeURIComponent(logoutUri)}`;
    };
  
    if (auth.isLoading) {
      return <div>Loading...</div>;
    }
  
    if (auth.error) {
      return <div>Encountering error... {auth.error.message}</div>;
    }
  
    if (auth.isAuthenticated) {
      return (
        <div>
          <pre> Hello: {auth.user?.profile.email} </pre>
          <pre> ID Token: {auth.user?.id_token} </pre>
          <pre> Access Token: {auth.user?.access_token} </pre>
          <pre> Refresh Token: {auth.user?.refresh_token} </pre>
  
          <button onClick={() => auth.removeUser()}>Sign out</button>
        </div>
      );
    }
  
    return (
      <div>
        <button onClick={() => auth.signinRedirect()}>Sign in</button>
        <button onClick={() => signOutRedirect()}>Sign out</button>
      </div>
    );
}

export default Register