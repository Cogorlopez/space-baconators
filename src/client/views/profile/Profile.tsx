import { useEffect, useState } from 'react'
import { useParams, Link } from 'react-router-dom'

import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import Grid from '@mui/material/Grid';

import { ENDPOINTS } from '../../../config/default';

const Profile = () => {
  const { userId } = useParams<{userId?: string}>()
  const [showsAttended, setShowsAttended] = useState<string[]>([])

  const fetchAttendance = async (userId) => {
    try {
      const response = await fetch(`${process.env.REACT_APP_SERVERLESS_API}${ENDPOINTS.ATTENDANCE}/${userId}`);
      const jsonData = await response.json();
      setShowsAttended(jsonData);
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    userId && void fetchAttendance(userId)
  }, [userId])

  return (
    <>
      <h1>{`${userId}'s Profile `}</h1>
      <Grid 
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justifyContent="center"
      >
        <Grid item>
        <Table >
          <TableHead sx={{fontSize: '22px'}}>{`Shows ${userId} has seen`}</TableHead>
          <TableBody>
            {showsAttended?.map(show => 
            <TableRow key={show}>
              <TableCell align="center" sx={{fontSize: '20px'}}>
                <Link
                  to={`${ENDPOINTS.SHOWS}/${show}`}
                  key={show}
                >
                  {show}
                </Link>
              </TableCell>
            </TableRow>
            )}
          </TableBody>
        </Table>
        </Grid>
      </Grid>
    </>
  )
}

export default Profile