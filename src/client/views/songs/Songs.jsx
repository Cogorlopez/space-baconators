import React from 'react'
import { useSelector } from 'react-redux'
import { getSongs } from '../../redux/selectors/songs'
import Row from 'react-bootstrap/Row'

const sortByName = (a, b) => {
  if (a.name < b.name) return -1
  if (a.name > b.name) return 1
  return 0
}

const Songs = () => {
  const songs = useSelector(state => getSongs(state))

  return (
    songs && (
      <>
        <Row className='d-flex justify-content-center'>
          <h1 className='my-4'>Songs</h1>
        </Row>
        <Row className='d-flex flex-column'>
          {songs.sort(sortByName).map(song => (
            <h3 key={song._id}>{song.name}</h3>
          ))}
        </Row>
      </>
    )
  )
}

export default Songs
