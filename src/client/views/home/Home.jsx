import Show from '../show/Show'

const Home = () => (
  <div className='d-flex flex-column justify-content-center mt-2'>
    <h1 style={{ margin: 'auto' }}>Latest Show</h1>
    <Show />
  </div>
)

export default Home
