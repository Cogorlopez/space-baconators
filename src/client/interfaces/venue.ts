export interface Venue {
  name: string
  state: string
  city: string
}