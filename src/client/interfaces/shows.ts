import { Venue } from './venue'

interface Show {
  showId: string
  date: string
  venue: Venue
  isLatest: boolean
  sets?: Record<string, any>[]
}

export default Show
