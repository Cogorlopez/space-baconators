import { useSelector } from 'react-redux'
import Show from '../interfaces/shows'
import showSelectors from '../redux/selectors/shows'

interface UseShowHook {
  shows: Show[]
  hasShows: Boolean
  displayShow: Show
  previousShow: Show
  nextShow: Show
  currentShowIndex: number,
  attendance: Record<string, Array<string>>
}

export const useShow = (): UseShowHook => {
  const shows = useSelector(state => showSelectors.getShows(state))
  const hasShows = shows.length > 0
  const displayShow = useSelector(state => showSelectors.getDisplayShow(state))
  const previousShow = useSelector(state =>
    showSelectors.getPreviousShow(state)
  )
  const nextShow = useSelector(state => showSelectors.getNextShow(state))
  const currentShowIndex = shows.findIndex(
    show => show?.date === displayShow?.date
  )
  const attendance = useSelector(state => (state as any)?.shows?.attendance)

  return {
    shows,
    hasShows,
    displayShow,
    previousShow,
    nextShow,
    currentShowIndex,
    attendance
  }
}
