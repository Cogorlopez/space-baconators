import { useEffect } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import Header from './components/header/Header.tsx'
import Songs from './views/songs/Songs'
import Home from './views/home/Home'
import Shows from './views/shows/Shows'
import Show from './views/show/Show'
import { useDispatch } from 'react-redux'
import { fetchShows } from './redux/actions/shows'
import Container from '@mui/material/Container';
import Loading from './components/loading/Loading'
import { useShow } from './hooks/useShow'
import Register from './views/register/Register'
import Profile from './views/profile/Profile'
import Callback from './views/callback/Callback'

const App = () => {
  const dispatch = useDispatch()
  const { hasShows } = useShow()

  useEffect(() => {
    if (hasShows) {
      return
    }
    dispatch(fetchShows())
  }, [hasShows, dispatch])
  
  return (
    <Router>
      <Header />
      <Container maxWidth={'lg'} className='d-flex-column justify-content-center'>
        <Switch>
          <Route exact path='/shows'>
            <Loading>
              <Shows />
            </Loading>
          </Route>
          <Route path={`/shows/:id`}>
            <Loading>
              <Show />
            </Loading>
          </Route>
          <Route exact path='/songs'>
            {/* TODO: implement a loading wrapper for links */}p
            <Loading>
              <Songs />
            </Loading>
          </Route>
          <Route path={`/register`}>
              <Register />
          </Route>
          <Route path={`/profile/:userId`}>
              <Profile />
          </Route>
          <Route path={`/callback`}>
              <Callback />
          </Route>
          <Route path='/'>
            <Loading>
              <Home />
            </Loading>
          </Route>
        </Switch>
      </Container>
    </Router>
  )
}

export default App
