import { createStore, combineReducers, compose, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { shows, songs, ui } from './reducers'

// Allows for app to still run if Redux DevTools is misssing from browser.
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(
  combineReducers({
    shows,
    songs,
    ui
  }),
  window.__DEFAULT_STATE,
  composeEnhancers(applyMiddleware(thunk))
)

export default store
