const defaultHandler = (state) => state

export const createReducer = (initialState, handlers) =>
  (state = initialState, action) => {
    const handler = handlers[action.type] || defaultHandler
    return handler(state, action)
  }
