const getSongs = (state) => state.songs.songs || []

module.exports = {
  getSongs
}
