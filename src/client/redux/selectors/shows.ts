import Show from '../../interfaces/shows'

const getShows = (state: any): Show[] => state.shows.shows ?? []

const getLatestShow = (state: any) =>
  getShows(state).find(show => show.isLatest)

const getLatestShowWithSets = (state: any) => {
  const shows = getShows(state)
  const filteredShows = shows.filter(show => show !== undefined)

  if (filteredShows.length === 0) {
    return
  }
  return filteredShows.find(show => show.sets && show.sets.length > 0)
}

const getDisplayShow = (state: any): Show => state.shows.displayShow

const getPreviousShow = (state: any): Show => {
  const currentShow = getDisplayShow(state)
  const shows = getShows(state)

  const currentShowIndex = shows.findIndex(
    show => show?.date === currentShow?.date
  )

  const previowsShow = shows[currentShowIndex + 1]

  return previowsShow
}

const getNextShow = (state: any): Show => {
  const currentShow = getDisplayShow(state)
  const shows = getShows(state)

  const currentShowIndex = shows.findIndex(
    show => show?.date === currentShow?.date
  )

  const previowsShow = shows[currentShowIndex - 1]

  return previowsShow
}

const showSelectors = {
  getShows,
  getLatestShow,
  getLatestShowWithSets,
  getDisplayShow,
  getPreviousShow,
  getNextShow
}

export default showSelectors
