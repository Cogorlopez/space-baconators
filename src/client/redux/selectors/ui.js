const getIsLoading = (state) => state.ui.isLoading

module.exports = {
  getIsLoading
}
