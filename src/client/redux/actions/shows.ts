import showSelectors from '../selectors/shows';
import { TYPES as UI_TYPES } from './ui';
import { ENDPOINTS } from '../../../config/default';
import { compareDesc } from 'date-fns';

export const TYPES = {
  SET_SHOWS: 'shows/setShows',
  SET_DISPLAY_SHOW: 'shows/setDisplayShow',
  SET_DISPLAY_SHOW_BY_DATE: 'shows/setDisplayShowByDate',
  SET_ATTENDANCE: 'shows/setAttendance'
};

export const setShows = (shows) => (dispatch) => {
  dispatch({
    type: TYPES.SET_SHOWS,
    shows
    // areShowsLoaded: true
  });
};

export const setDisplayShow = (show) => (dispatch) => {
  dispatch({
    type: TYPES.SET_DISPLAY_SHOW,
    show
  });
};

export const fetchShows = () => async (dispatch) => {
  dispatch({ type: UI_TYPES.SET_IS_LOADING, isLoading: true });

  try {
    const response = await fetch(`${process.env.REACT_APP_SERVERLESS_API}${ENDPOINTS.SHOWS}`);
    if (!response.ok) {
      dispatch({ type: UI_TYPES.SET_IS_LOADING, isLoading: false });
      throw Error('Getting shows failed');
    }
    const shows = await response.json();
    const sortedShows = shows.sort((a, b) =>
      compareDesc(new Date(a.date), new Date(b.date))
    );
    // const displayShow = sortedShows[0];
  
    // const attendeesResponse = await fetch(`${process.env.REACT_APP_SERVERLESS_API}${ENDPOINTS.ATTENDEES}/${displayShow.date}`);

    
    // if (!attendeesResponse.ok) {
    //   dispatch({ type: UI_TYPES.SET_IS_LOADING, isLoading: false });
    //   throw new Error(response.statusText)
    // }

    // const displayShowWithAttendees = {
    //   ...displayShow,
    //   attendees: await attendeesResponse.json()
    // }
    
    dispatch({ type: UI_TYPES.SET_IS_LOADING, isLoading: false });
    dispatch(setShows(sortedShows));
    // dispatch({ type: TYPES.SET_DISPLAY_SHOW, show: displayShowWithAttendees });
  } catch(err) {
    dispatch({ type: UI_TYPES.SET_IS_LOADING, isLoading: false });
    console.error(err)
  }
};

export const fetchShowById = (showId: string) => async (dispatch: any) => {
  dispatch({ type: UI_TYPES.SET_IS_LOADING, isLoading: true });

  try {
    const response = await fetch(`${process.env.REACT_APP_SERVERLESS_API}${ENDPOINTS.SHOWS}/${showId}`);
    if (!response.ok) {
      dispatch({ type: UI_TYPES.SET_IS_LOADING, isLoading: false });
      throw Error('Getting show failed');
    }
    dispatch({ type: UI_TYPES.SET_IS_LOADING, isLoading: false });
    const show = await response.json();

  
    dispatch(setShows([{...show, isLatest: true}]));
  } catch(err) {
    console.error(err)
  }
}

export const setDisplayShowByDate = (showDate) => async (dispatch, getState) => {
  if (!showDate) {
    const latestShow = showSelectors.getLatestShowWithSets(getState());
    dispatch(setDisplayShow(latestShow));
    return;
  }
  const shows = showSelectors.getShows(getState());
  const displayShow = shows.find(
    (show) => show.date.substr(0, 10) === showDate
  );

  dispatch({ type: TYPES.SET_DISPLAY_SHOW, show: displayShow });
};

export const setDisplayShowToLatest = () => (dispatch, getState) => {
  const latestShow = showSelectors.getLatestShowWithSets(getState());
  dispatch(setDisplayShow(latestShow));
};

export const fetchShowAttendance = (showDate: string) => async (dispatch, getState) => {
  
  if (!showDate) {
    return
  }

  try {
    const response = await fetch(`${process.env.REACT_APP_SERVERLESS_API}${ENDPOINTS.ATTENDEES}/${showDate}`);
    
    if (!response.ok) {
      throw new Error(response.statusText)
    }
    
    const attendees = await response.json()

    dispatch({ type: TYPES.SET_ATTENDANCE, attendanceList: attendees, showDate });
  } catch (err) {
    console.error(err)
  }
}
