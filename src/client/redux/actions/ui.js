
export const TYPES = {
  SET_IS_LOADING: 'ui/setIsLoading'
}

export const setIsLoading = (isLoading) => dispatch => {
  dispatch({
    type: TYPES.SET_IS_LOADING,
    isLoading
  })
}
