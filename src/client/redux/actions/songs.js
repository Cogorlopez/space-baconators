export const TYPES = {
  SET_SONGS: 'songs/setSongs',
  GET_SONGS: 'songs/fetchSongs'
};

export const setSongs = (songs) => (dispatch) => {
  dispatch({
    type: TYPES.SET_SONGS,
    songs
  });
};

export const fetchSongs = () => async (dispatch) => {
  const response = await fetch('/api/songs');
  if (!response.ok) throw Error('Getting songs failed');
  dispatch({ type: TYPES.SET_SONGS, songs: await response.json() });
};
