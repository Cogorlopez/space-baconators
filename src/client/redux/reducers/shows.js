import { TYPES } from '../actions/shows'
import { createReducer } from '../utils'

const DEFAULT_STATE = {
  shows: [],
  areShowsLoaded: false,
  // displayShow: {},
  attendance: {}
}

const setShows = (state, { shows }) => {
  return {
    ...state,
    shows: [...Object.values(shows)]
  }
}

const setDisplayShow = (state, { show }) => {
  return {
    ...state,
    displayShow: show
  }
}

const setAttendance = (state, { attendanceList, showDate }) => {
  return {
    ...state,
    attendance: {
      [showDate]: attendanceList

    }
  }
}

const shows = createReducer(DEFAULT_STATE, {
  [TYPES.SET_SHOWS]: setShows,
  [TYPES.SET_DISPLAY_SHOW]: setDisplayShow,
  [TYPES.SET_ATTENDANCE]: setAttendance
})

export default shows
