import { TYPES } from '../actions/ui'
import { createReducer } from '../utils'
const DEFAULT_STATE = {
  isLoading: false
}

const setIsLoading = (state, { isLoading }) => {
  return {
    ...state,
    isLoading: isLoading
  }
}

const ui = createReducer(DEFAULT_STATE, {
  [TYPES.SET_IS_LOADING]: setIsLoading
})

export default ui
