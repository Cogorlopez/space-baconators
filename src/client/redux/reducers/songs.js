import { TYPES } from '../actions/songs'
import { createReducer } from '../utils'
const DEFAULT_STATE = {
  songs: []
}

const setSongs = (state, { songs }) => {
  return {
    ...state,
    songs: [...songs]
  }
}

const songs = createReducer(DEFAULT_STATE, {
  [TYPES.SET_SONGS]: setSongs
})

export default songs
