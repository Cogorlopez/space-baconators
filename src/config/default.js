module.exports = {
  APP_NAME: 'ma-space-baconators',
  ENDPOINTS: {
    SHOWS: '/shows',
    SONGS: '/songs',
    ATTENDEES: '/attendees',
    ATTENDANCE: '/attendance'
  }
}
