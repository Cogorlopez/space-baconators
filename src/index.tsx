import React from 'react';
import { createRoot } from 'react-dom/client'
import { ThemeProvider, createTheme } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { AuthProvider } from "react-oidc-context";

import App from './client/App'
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux'
import store from './client/redux/store'

import './index.css';

const cognitoAuthConfig = {
  authority: process.env.REACT_APP_AUTHORITY,
  client_id: process.env.REACT_APP_CLIENT_ID,
  redirect_uri: `${process.env.REACT_APP_BASE_URL}/callback`,
  response_type: "code",
  scope: "phone openid email",
};


const container = document.getElementById('root')
const root = createRoot(container)

const darkTheme = createTheme({
  palette: {
    mode: 'dark',
  },
  typography: {
    fontFamily: ["Anta", 'sans-serif'].join(','),
  }
});


root.render(
  <React.StrictMode>
    <ThemeProvider theme={darkTheme}>
      <AuthProvider {...cognitoAuthConfig}>
        <Provider store={store}>
        <CssBaseline />
          <App />
        </Provider>
      </AuthProvider>
    </ThemeProvider>
  </React.StrictMode>,
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
